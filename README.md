# Whnex

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## [DEV] Launch with docker and docker compose

```sh
docker login registry.gitlab.com

make pull
make up
```

## [DEV] Launch WITHOUT docker and docker compose, using ASDF

Install [asdf](https://asdf-vm.com/guide/getting-started.html)

```sh
asdf install
make up-no-docker
```

## Upgrade app (following mix.exs versions)
```sh
make exec-bash
mix deps.update --all
```

## [PROD] Launch with docker and docker compose

1. Change variables in your dotenv file (.env.local)  
2. Create caddy volume 
```sh
docker volumes create --name=caddy_data
```
3. Launch
```sh
docker compose -d -f docker-compose.prod.yaml up
```

## [PROD] Upgrade Dockerfile
```sh
make exec-bash
mix phx.gen.release --docker
cp Dockerfile ./docker/Dockerfile
```


## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix

