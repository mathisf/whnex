up: 
	docker compose -f docker-compose.yaml up 

up-no-docker:
	./docker/entrypoint/elixir.sh

down: 
	docker compose -f docker-compose.yaml down

init: pull up

build:
	docker compose -f docker-compose.yaml build 

build-prod:
	docker compose -f docker-compose.prod.yaml --env-file .env.local build

pull:
	docker compose -f docker-compose.yaml pull 

pull-prod:
	docker compose -f docker-compose.prod.yaml --env-file .env.local pull

push:
	docker compose -f docker-compose.yaml push 

push-prod:
	docker compose -f docker-compose.prod.yaml --env-file .env.local push

build-and-push: build push

build-prod-and-push: build-prod push-prod
build-and-push-prod: build-prod push-prod

exec-bash:
	docker compose exec elixir sh

asdf:
	asdf plugin add erlang
	asdf plugin add elixir
	asdf install