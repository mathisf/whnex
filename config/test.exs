import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :whn, WhnWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "AC/u9K6byWF/Bdbsa0Nz47pNSfNSlCpp42YDWE20dOBPz1JJY5jIYPu0g6eufo3E",
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
