# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

# Configures the endpoint
config :whn, WhnWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: WhnWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Whn.PubSub,
  live_view: [signing_salt: "iKE+Cwnj"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :whn, WhnWeb.Meta, %{
  title: "whnex",
  description: "Fast and simple Hacker news client",
  tags: "whnex,whn,hn,hacker news,hacker,news,mathisfaiv.re,elixir,phoenix"
}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
