#!/bin/bash

mix local.hex --force
mix local.rebar --force

# Compile the project.
mix deps.get --all
mix do compile

exec mix phx.server