function toggleKids(itemId, directKidIds, allKidIds) {
    itemElem = document.getElementById(itemId);
    areKidsDisplayed = itemElem.getAttribute('are-kids-displayed') === 'true';
    buttonElem = document.getElementById('button-' + itemId);
    buttonElem.innerHTML = buttonElem.innerHTML.trim().replace(/^.{4}/g, !areKidsDisplayed ? 'Hide' : 'Show');
    itemElem.setAttribute('are-kids-displayed', !areKidsDisplayed ? "true" : "false");
    
    kidIds = areKidsDisplayed ? allKidIds : directKidIds
    for (const kidId of kidIds) {
        kidElem = document.getElementById(kidId);
        if (!kidElem) {
            continue;
        }
        kidElem.style.display = areKidsDisplayed ? "none" : "flex";
    }
}