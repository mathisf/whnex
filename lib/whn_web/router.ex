defmodule WhnWeb.Router do
  use WhnWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", WhnWeb do
    pipe_through :browser

    get "/", ItemController, :index
    get "/search", SearchController, :show
    get "/about", AboutController, :index
    get "/items/:item", ItemController, :show
    get "/users/:user", UserController, :show
    get "/time/:list_name", TimeController, :index
    get "/:list_name", ItemController, :index
  end

  scope "/api", WhnWeb do
    pipe_through :api

    get "/", ApiItemController, :index
    get "/:list_name", ApiItemController, :index
    get "/items/:item", ApiItemController, :show
  end

  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/admin" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: WhnWeb.Telemetry
    end
  end
end
