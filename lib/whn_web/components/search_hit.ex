defmodule WhnWeb.Components.SearchHit do
  use WhnWeb, :component

  def search_hit(assigns) do
    href = "/items/" <> to_string(assigns.item.objectID)


    ~H"""
    <div class="story">
      <div>
        <%= raw get_title(@item) %>
        <div class="story-details">
          by
          <a href={"/users/" <> to_string(@item.author)}><%= @item.author %></a>,
          <%= Whn.TextProcessor.calculate_time_ago(@item.created_at_i) %>
          <a class="comments-details" rel="prefetch" href={href}>
            view <%= to_string(assigns.item.num_comments) %> comments
          </a>
        </div>
      </div>
    </div>
    """
  end

  defp blank?(str_or_nil) do
    "" == str_or_nil |> to_string() |> String.trim()
  end

  defp get_title(item) do
    cond do
      !blank?(item.comment_text) ->
        item.comment_text
      !blank?(item.story_text) ->
        item.story_text
      !blank?(item.title) ->
        "<a href=\"" <> to_string(item.url) <> "\">" <> to_string(item.title) <> "</a>"
    end
  end
end
