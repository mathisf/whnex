defmodule WhnWeb.Components.Navbar do
  use WhnWeb, :component
  use Phoenix.Controller

  def navbar(assigns) do
    ~H"""
    <nav role="navigation">
      <span>
        <.navbar_item controller_name={"item"} controller_action={"index"} path={"top"} current_path={@current_path} conn={@conn}/>
        <.navbar_item controller_name={"item"} controller_action={"index"} path={"best"} current_path={@current_path} conn={@conn} />
        <.navbar_item controller_name={"item"} controller_action={"index"} path={"show"} current_path={@current_path} conn={@conn}  />
        <.navbar_item controller_name={"item"} controller_action={"index"} path={"ask"} current_path={@current_path} conn={@conn} />
        <.navbar_item controller_name={"item"} controller_action={"index"} path={"new"} current_path={@current_path} conn={@conn} />
        <span><%= link "about", to: Routes.about_path(@conn, :index), class: "#{if @current_path == "about", do: "active"}" %></span>
        <%= if function_exported?(Routes, :live_dashboard_path, 2) do %>
          <span><%= link "LiveDashboard", to: Routes.live_dashboard_path(@conn, :home) %></span>
        <% end %>
      </span>
      <span>
      <%= if Atom.to_string(action_name(@conn)) == "index" and (
          String.contains?(Atom.to_string(controller_module(@conn)), "ItemController") or
          String.contains?(Atom.to_string(controller_module(@conn)), "TimeController")) do %>
        <a href={if String.starts_with?(current_path(@conn), "/time"), do: Routes.item_path(@conn, :index, @current_path), else: Routes.time_path(@conn, :index, @current_path) }>
          <i class={if String.starts_with?(current_path(@conn), "/time"), do: "iconoir-clock icon-orange", else: "iconoir-clock icon-white"} title="Sort by time"></i>
        </a>
      <% end %>
        <span>
          <%= form_for @conn, Routes.search_path(@conn, :show), [method: :get], fn f -> %>
            <%= search_input f, :query, [placeholder: "search..."] %>
          <% end %>
        </span>
      </span>
    </nav>
    """
  end

  def navbar_item(assigns) do
    class = "navbar-item #{if assigns[:current_path] == assigns.path, do: "active"}"

    ~H"""
    <span>
    <%=
      link @path,
      to: apply(
        Routes,
        String.to_atom("#{@controller_name}_path"),
        [
          @conn,
          String.to_atom(@controller_action),
          @path,
        ]
      ),
      class: class
      %>
      </span>
      &nbsp;
    """
  end
end
