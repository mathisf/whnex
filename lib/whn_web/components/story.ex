defmodule WhnWeb.Components.Story do
  use WhnWeb, :component

  def story(assigns) do
    points_comments_title = to_string(assigns.item.descendants) <> "comment(s), click to view"
    href = "/items/" <> to_string(assigns.item.id)

    ~H"""
    <div class="story">
      <div class="index"><%= @index %></div>
      <a
        title={points_comments_title}
        class="points-comments"
        rel="prefetch"
        href={href}>
        <div>
          <i class="iconoir-arrow-up white-icon"></i>
          <span>
            <%= if is_nil(@item.score) do %>
              0
            <% else %>
              <%= @item.score %>
            <% end %>
          </span>
        </div>
        <div>
          <i class="iconoir-message-text white-icon"></i>
          <span><%= if not is_nil(@item.descendants), do: to_string(@item.descendants) %></span>
        </div>
      </a>
      <div class="story-content">
        <a
          class="story-title"
          href={@item.url || href}
          rel={if Whn.TextProcessor.get_host_name(@item.url || "") == "", do: "prefetch", else: "external" }>
            <%= raw @item.title%>
          <%= if Whn.TextProcessor.get_host_name(@item.url || "") != "" do %>
            <small class="story-url"><%= Whn.TextProcessor.get_host_name(@item.url || "") %></small>
          <% end %>
        </a>

        <div class="story-details">
          by
          <a href={"/users/" <> to_string(@item.by)}><%= @item.by %></a>
          <%= Whn.TextProcessor.calculate_time_ago(@item.time) %>
          <a class="comments-details" rel="prefetch" href={href}>
            <%= to_string(@item.descendants || 0) %> comments
          </a>
        </div>
      </div>
    </div>
    """
  end
end
