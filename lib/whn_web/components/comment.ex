defmodule WhnWeb.Components.Comment do
  use WhnWeb, :component

  @colors %{
    0 => "rgb(161, 56, 46)",
    1 => "rgb(161, 56, 46)",
    2 => "rgb(165, 91, 37)",
    3 => "rgb(145, 120, 54)",
    4 => "rgb(63, 110, 79)",
    5 => "rgb(53, 102, 171)",
    6 => "rgb(42, 69, 131)",
    7 => "rgb(94, 61, 144)"
  }

  def comment(assigns) do
    level = assigns.level
    is_grand_kid = level > 0
    class = "comment level-" <> to_string(level)
    style = get_comment_style(level)
    bar_style = if is_grand_kid, do: "background-color: " <> @colors[rem(level, 7)], else: ""
    replies_button_id = "button-" <> to_string(assigns.item.id)

    onclick =
      "toggleKids(" <>
        to_string(assigns.item.id) <>
        ", [" <>
        Enum.join(
          get_direct_kid_ids(assigns.kids),
          ", "
        ) <>
        "], [" <>
        Enum.join(
          get_all_kid_ids(assigns.kids),
          ", "
        ) <>
        "])"

    ~H"""
      <div class={class} style={style} id={@item.id} are-kids-displayed={!is_grand_kid}>
        <%= if @level >= 1 do %>
          <div
            class="color-bar"
            style={bar_style}>
            &nbsp;
          </div>
        <% end %>
        <div>
          <div class="comment-header">
            <i>
              <a href={"/users/" <> to_string(@item.by)}
                class={if @op === @item.by, do: "op", else: ""}
                title={if @op === @item.by, do: "op", else: ""}
              >
                <%= @item.by %>
              </a>
            </i>
            <i><%= Whn.TextProcessor.calculate_time_ago(@item.time) %></i>
          </div>
          <div class="comment-content">
            <%= raw Whn.TextProcessor.process_comment_text(@item.text, @conn) %>
          </div>
          <div class="toggle-replies">
            <%= if Enum.count(@item.kids || []) > 0 && level != -1 do %>
              <div onclick={onclick} id={replies_button_id}>
                Show <%= Enum.count(@item.kids || []) %> repl<%= if (Enum.count(@item.kids || []) == 1), do: "y", else: "ies" %>
              </div>
            <% end %>
          </div>
        </div>
      </div>
    """
  end

  def comment_hierarchy(assigns) do
    ~H"""
    <%= if !is_nil(@item.text) && !@item.deleted  do %>
      <.comment op={@op} level={@level} item={@item} kids={@kids} conn={@conn}/>
    <% end %>
    <%= for kid <- @kids do %>
      <%= if !kid.item.deleted && !is_nil(kid.item.text) do %>
        <.comment_hierarchy op={@op} level={@level + 1} item={kid.item} kids={kid.kids} conn={@conn}/>
      <% end %>
    <% end %>
    """
  end

  def show_user_comment(assigns) do
    ~H"""
    <div>
      <div class="comment-header">
        <i></i>
        <i><%= Whn.TextProcessor.calculate_time_ago(@item.time) %></i>
      </div>
      <div class="comment-content">
        <%= raw Whn.TextProcessor.process_comment_text(@item.text, @conn) %>
      </div>
    </div>
    """
  end

  defp get_all_kid_ids(kids) do
    List.flatten(
      Enum.map(kids, fn kid -> [to_string(kid.item.id) | get_all_kid_ids(kid.kids)] end)
    )
  end

  defp get_direct_kid_ids(kids) do
    Enum.map(kids, fn kid -> to_string(kid.item.id) end)
  end

  defp get_comment_style(level) when level >= 0 do
    "margin-left: " <>
      to_string(level * 10) <>
      "px;" <>
      if level > 0, do: " display: none;", else: ""
  end

  defp get_comment_style(level) when level < 0 do
    if level > 0, do: " display: none;", else: ""
  end
end
