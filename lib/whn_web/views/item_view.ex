defmodule WhnWeb.ItemView do
  use WhnWeb, :view

  def metadata(:index, assigns), do: %{
    title: "whnex - " <> assigns.list_name
  }

  def metadata(:show, assigns) do
    %{
      title: "whnex - " <> (assigns.item.title || Integer.to_string(assigns.item.id))
    }
  end
end
