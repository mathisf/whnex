defmodule WhnWeb.SearchView do
  use WhnWeb, :view

  def metadata(:show, assigns), do: %{
    title: "whnex - search: " <> assigns.query
  }

end
