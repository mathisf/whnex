defmodule WhnWeb.UserView do
  use WhnWeb, :view

  def metadata(:show, assigns), do: %{
    title: "whnex - user: " <> assigns.user.id
  }
end
