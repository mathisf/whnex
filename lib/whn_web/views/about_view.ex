defmodule WhnWeb.AboutView do
  use WhnWeb, :view

  def metadata(:index, _), do: %{
    title: "whnex - about"
  }
end
