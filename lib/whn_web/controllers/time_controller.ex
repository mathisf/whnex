defmodule WhnWeb.TimeController do
  use WhnWeb, :controller

  def index(conn, params) do
    list_name = Map.get(params, "list_name", "top")

    case Enum.member?(["top", "best", "show", "ask", "new"], list_name) do
      false ->
        conn
          |> put_status(:not_found)
          |> put_view(WhnWeb.ErrorView)
          |> render(:"404")
      true ->
        items =
          Whn.ApiConsumer.Official.get_list(list_name)
            |> Enum.map(&to_string(&1))
            |> Task.async_stream(
              &Whn.ApiConsumer.Official.get_item(&1),
              timeout: :infinity,
              on_timeout: :kill_task
            )
            |> Enum.to_list()
            |> Enum.map(fn {:ok, item} -> item end)
            |> Enum.reduce(Map.new, fn (item, acc) ->
              if item.time > 0 do
                {:ok, date} = DateTime.from_unix(item.time)
                {:ok, key} = Timex.format(date, "{ISOdate}")
                Map.update(
                  acc,
                  String.to_atom(key),
                  [item],
                  fn existingItems -> [item | existingItems] |> Enum.sort_by(&(&1.time), :desc) end
                )
              end
            end)
            |> Enum.sort_by(& &1, :desc)

          render(conn, "index.html", list_name: list_name, time_sorted_items: items)
    end


  end

  def show(conn, %{"item" => item_id} = _params) do
    case String.match?(item_id, ~r/^[[:digit:]]+$/) do
      false ->
        conn
          |> put_status(:not_found)
          |> put_view(WhnWeb.ErrorView)
          |> render(:"404")
      true ->
        item = Whn.ApiConsumer.Official.get_item(item_id)

        render(conn, "show.html", item: item, kids: get_kids(item.kids || []))
    end
  end

  defp get_kids(kids_ids) do
    (kids_ids || [])
    |> Enum.map(&to_string(&1))
    |> Task.async_stream(
      fn kid_id ->
        item = Whn.ApiConsumer.Official.get_item(kid_id)
        kids = get_kids(item.kids)

        %{item: item, kids: kids}
      end,
      timeout: :infinity,
      on_timeout: :kill_task
    )
    |> Enum.to_list()
    |> Enum.map(fn {:ok, kid} -> kid end)
  end
end
