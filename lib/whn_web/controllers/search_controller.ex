defmodule WhnWeb.SearchController do
  use WhnWeb, :controller

  def show(conn, %{"query" => query} = _params) do
    result = Whn.ApiConsumer.Official.search(query)

    render(conn, "show.html", hits: result.hits, query: query)
  end
end
