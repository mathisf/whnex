defmodule WhnWeb.ApiItemController do
  use WhnWeb, :controller

  def index(conn, params) do
    list_name = Map.get(params, "list_name", "top")

    case Enum.member?(["top", "best", "show", "ask", "new"], list_name) do
      false ->
        conn
          |> put_status(:not_found)
          |> render(:"404")
      true ->
        items =
          Whn.ApiConsumer.Official.get_list(list_name)
            |> Enum.map(&to_string(&1))
            |> Task.async_stream(
              &Whn.ApiConsumer.Official.get_item(&1),
              timeout: :infinity,
              on_timeout: :kill_task
            )
            |> Enum.to_list()
            |> Enum.map(fn {:ok, kid} -> kid end)

        conn
          |> json(items)
    end
  end

  def show(conn, %{"item" => item_id} = _params) do
    case String.match?(item_id, ~r/^[[:digit:]]+$/) do
      false ->
        conn
          |> put_status(:not_found)
          |> render(:"404")
      true ->
        item = Whn.ApiConsumer.Official.get_item(item_id)
        item = %Item{ item | kids: get_kids(item.kids) }

        conn
          |> json(item)
    end
  end

  defp get_kids(kids_ids) do
    (kids_ids || [])
    |> Enum.map(&to_string(&1))
    |> Task.async_stream(
      fn kid_id ->
        item = Whn.ApiConsumer.Official.get_item(kid_id)
        if !is_nil(item) and item !== { :ok, nil } do
          kids = get_kids(item.kids)
          %{item: item, kids: kids}
        else
          %{item: nil, kids: []}
        end
      end,
      timeout: :infinity,
      on_timeout: :kill_task
    )
    |> Enum.to_list()
    |> Enum.map(fn {:ok, kid} -> kid end)
    |> Enum.filter(fn kid -> !is_nil(kid.item) and kid.item !== { :ok, nil } end)
  end
end
