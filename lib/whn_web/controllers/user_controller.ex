defmodule WhnWeb.UserController do
  use WhnWeb, :controller

  def show(conn, %{"user" => username} = _params) do
    user = Whn.ApiConsumer.Official.get_user(username)

    render(conn, "show.html", user: user, submitted: get_items(user.submitted || []))
  end

  defp get_items(item_ids) do
    (item_ids || [])
    |> Enum.map(&to_string(&1))
    |> Enum.take(5) # FIXME Too slow
    |> Task.async_stream(
      fn item_id ->
        Whn.ApiConsumer.Official.get_item(item_id)
      end,
      timeout: :infinity,
      on_timeout: :kill_task
    )
    |> Enum.to_list()
    |> Enum.map(fn {:ok, item} -> item end)
    |> sort_items()
  end

  defp sort_items(items) do
    %{
      stories: items |> Enum.filter(fn item -> item.type == "story" end),
      comments: items |> Enum.filter(fn item -> item.type == "comment" end)
    }
  end
end
