defmodule WhnWeb.AboutController do
  use WhnWeb, :controller

  def index(conn, _) do
    render(conn, "index.html", path: "about")
  end
end
