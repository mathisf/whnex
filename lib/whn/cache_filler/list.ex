defmodule Whn.CacheFiller.List do
  use GenServer

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    fill()
    # Schedule work to be performed at some point
    schedule_fill()
    {:ok, state}
  end

  def handle_info(:fill_lists_cache, state) do
    fill()
    # Reschedule once more
    schedule_fill()
    {:noreply, state}
  end

  defp schedule_fill() do
    # refresh lists every 5 minutes
    Process.send_after(self(), :fill_lists_cache, 5 * 60 * 1000)
  end

  defp fill do
    ["top", "best", "ask", "show", "job", "new"]
    |> Enum.map(&Whn.ApiConsumer.Official.get_list(&1, false))
  end
end
