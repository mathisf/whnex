defmodule Whn.CacheFiller.Item do
  use GenServer

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    fill()
    # Schedule work to be performed at some point
    schedule_fill()
    {:ok, state}
  end

  def handle_info(:fill_items_cache, state) do
    fill()
    # Reschedule once more
    schedule_fill()
    {:noreply, state}
  end

  defp schedule_fill() do
    # In 15 minutes
    Process.send_after(self(), :fill_items_cache, 15 * 60 * 1000)
  end

  defp fill do
    all_item_ids =
      ["top", "best", "ask", "show", "job", "new"]
      |> Enum.map(&Whn.ApiConsumer.Official.get_list(&1, false))
      |> Enum.concat()
      |> Enum.uniq()

    Task.async_stream(
      all_item_ids,
      &fill_item_and_kids(to_string(&1)),
      timeout: :infinity,
      on_timeout: :kill_task
    )
    |> Stream.run()
  end

  defp fill_item_and_kids(parent_item_id) do
    item = Whn.ApiConsumer.Official.get_item(parent_item_id, false)

    case item do
      %{ kids: kids } -> Task.async_stream(
          kids || [],
          &fill_item_and_kids(to_string(&1)),
          timeout: :infinity,
          on_timeout: :kill_task
        )
        |> Stream.run()
      _ -> nil
    end
  end
end
