defmodule Whn.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      WhnWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Whn.PubSub},
      # Start the Endpoint (http/https)
      WhnWeb.Endpoint,
      # Start a worker by calling: Whn.Worker.start_link(arg)
      # {Whn.Worker, arg}
      {Cachex, name: :hn_api_cache},
      Whn.ApiConsumer.Official.child_spec(),
      {Whn.CacheFiller.List, name: Whn.CacheFiller.List, restart: :transient},
      {Whn.CacheFiller.Item, name: Whn.CacheFiller.Item, restart: :transient}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Whn.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    WhnWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
