# TODO

# story
# function transformUrl(url) {
#   if (url.substr(0, 8) === "item?id=") return "items/" + url.substr(8);

#   return url;
# }

# function getHostname(url) {
#   var match = url.match(
#     /^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/
#   );

#   if (!match) return "";

#   let hostname = match[3];

#   return hostname.substr(0, 4) === "www." ? hostname.substr(4) : hostname;
# }

# comment
# // Transform HN links to whn links
#   comment.content = comment.content.replace(
#     /https?\:(&#x2F;|\/){2}news\.ycombinator\.com(&#x2F;|\/)item\?id\=/gim,
#     window.location.protocol + "//" + window.location.host + "/items/"
#   );

#   if (get(greentextQuote)) {
#     // Greentext, paragraphes starting with > will be green
#     let htmlComment = parser.parseFromString(comment.content, "text/html")
#       .documentElement;

#     for (let element of htmlComment.getElementsByTagName("p")) {
#       element.innerHTML = element.innerHTML.replace(
#         /^\s*(&gt;.*)/g,
#         '<i style="color: #a4e82c;">$1</i>'
#       );
#     }

#     comment.content = htmlComment.getElementsByTagName("body")[0].innerHTML;
#   }
defmodule Whn.TextProcessor do
  def calculate_time_ago(time) when time > 0 do
    {:ok, date} = DateTime.from_unix(time)

    Timex.from_now(date)
  end

  def calculate_time_ago(time) when time <= 0 do
    "a few seconds ago"
  end

  def get_host_name(url) do
    captures =
      Regex.run(
        ~r/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/,
        url
      )

    case captures == nil do
      true ->
        ""

      false ->
        String.replace(Enum.at(captures, 3), "www.", "")
    end
  end

  def process_comment_text(text, conn) do
    text
    |> replace_official_hn_url(conn)
  end

  defp replace_official_hn_url(text, conn) do
    Regex.replace(
      ~r/https?\:(&#x2F;|\/){2}news\.ycombinator\.com(&#x2F;|\/)item\?id\=/mi,
      text || "",
      Atom.to_string(conn.scheme) <>
        "://" <> (Enum.into(conn.req_headers, %{}) |> Map.get("host")) <> "/items/",
      global: true
    )
  end
end
