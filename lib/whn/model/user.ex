defmodule User do
  defstruct [
    :about,
    :created,
    :delay,
    :id,
    :karma,
    :submitted,
  ]
end
