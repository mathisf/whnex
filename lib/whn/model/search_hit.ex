defmodule SearchHit do
  defstruct [
    :id,
    :created_at,
    :created_at_i,
    :author,
    :title,
    :url,
    :points,
    :text,
    :type,
    :parent_id,
    :story_id,
    :children,
    :objectID,
    :num_comments,
    :comment_text,
    :story_text,
    :_highlightResult,
    :_tags,
  ]
end
