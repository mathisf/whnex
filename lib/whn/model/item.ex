defmodule Item do
  @derive Jason.Encoder
  defstruct [
    :id,
    :descendants,
    :by,
    :kids,
    :score,
    :time,
    :text,
    :title,
    :url,
    :type,
    :dead,
    :deleted,
    :parts,
  ]
end
