defmodule Search do
  defstruct [
    :hits,
    :page,
    :nbHits,
    :nbPages,
    :hitsPerPage,
    :processingTimeMS,
    :query,
    :params,
  ]
end
