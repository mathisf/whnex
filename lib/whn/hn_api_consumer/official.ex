defmodule Whn.ApiConsumer.Official do
  alias Finch.Response
  def pool_size, do: 10000
  def pool_count, do: 1
  def child_spec do
    {Finch,
      name: __MODULE__,
      pools: %{
        "https://hacker-news.firebaseio.com" => [
          size: pool_size(),
          count: pool_count(),
          conn_max_idle_time: :timer.minutes(10),
        ],
        "https://hn.algolia.com" => [
          size: pool_size(),
          count: pool_count(),
          conn_max_idle_time: :timer.minutes(10),
        ],
      },
      restart: :transient,
    }
  end


  def get_item(item_id, use_cache \\ true) do
    case use_cache do
      true ->
        {_, item} =
          Cachex.fetch(:hn_api_cache, item_id, fn -> {:commit, request_item(item_id)} end)

        item

      false ->
        request_item(item_id)
    end
  end

  def get_list(list_name \\ "top", use_cache \\ true) do
    case use_cache do
      true ->
        {_, list} =
          Cachex.fetch(:hn_api_cache, list_name, fn -> {:commit, request_list(list_name)} end)

        list

      false ->
        request_list(list_name)
    end
  end

  def get_user(username, use_cache \\ true) do
    case use_cache do
      true ->
        {_, user} =
          Cachex.fetch(:hn_api_cache, "u_" <> username, fn ->
            {:commit, request_user(username)}
          end)

        user

      false ->
        request_user(username)
    end
  end

  defp request_user(username) do
    response =
      :get
      |> Finch.build("https://hacker-news.firebaseio.com/v0/user/" <> username <> ".json")
      |> Finch.request(__MODULE__)

    case response do
      {:ok, %Response{status: 200, body: body}} ->
        user = body |> Poison.decode!(%{keys: :atoms, as: %User{}})
        Cachex.put(:hn_api_cache, "u_" <> username, user)
        Cachex.expire(:hn_api_cache, "u_" <> username, :timer.hours(24 * 5))
        user
    end
  end

  defp request_item(item_id) do
    response =
      :get
      |> Finch.build("https://hacker-news.firebaseio.com/v0/item/" <> item_id <> ".json")
      |> Finch.request(__MODULE__)

    case response do
      {:ok, %Response{status: 200, body: body}} ->
        item = body |> Poison.decode!(%{keys: :atoms, as: %Item{}})
        Cachex.put(:hn_api_cache, item_id, item)
        Cachex.expire(:hn_api_cache, item_id, :timer.hours(24 * 5))
        item
      _ ->
        Cachex.get(:hn_api_cache, item_id)
    end
  end

  defp request_list(list_name) do
    response =
      :get
      |> Finch.build("https://hacker-news.firebaseio.com/v0/" <> list_name <> "stories.json")
      |> Finch.request(__MODULE__)

    case response do
      {:ok, %Response{status: 200, body: body}} ->
        list = body |> Poison.decode!()
        Cachex.put(:hn_api_cache, list_name, list)
        list
      _ ->
        Cachex.get(:hn_api_cache, list_name)
    end
  end

  def search(query) do
    response =
      :get
      |> Finch.build("https://hn.algolia.com/api/v1/search?" <> URI.encode_query(%{query: query, hitsPerPage: 100}))
      |> Finch.request(__MODULE__)

    case response do
      {:ok, %Response{status: 200, body: body}} ->
        search = body |> Poison.decode!(%{keys: :atoms, as: %Search{}})
        search
    end
  end
end
